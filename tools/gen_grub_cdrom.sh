#!/usr/bin/env bash

# Ensure to abort on error
set -e

wai=$(dirname $(readlink -f "$0")) # Current script directory
outdir="${wai}/../"
cdrom="${outdir}/cdrom.img"
isodir="$(mktemp -d)" # Mount point (where the floppy will be mounted temporally
kernel="$outdir/src/bringelle"

[ ! -e "$kernel" ] && { echo "Bringelle not found!"; exit 1; } 

check_for () {
    command -v "$1" &>/dev/null || { echo "Command $1 not found!"; exit 1; }
}

check_for grub-mkconfig

mkdir -p $isodir/boot/grub
cat <<EOT >> $isodir/boot/grub/grub.cfg
set timeout=0
menuentry "kernel" {
    multiboot2 /boot/bringelle
    boot
}
EOT
cp $kernel "$isodir/boot/"
grub-mkrescue -o "$cdrom" $isodir

rm -rf "$isodir"
