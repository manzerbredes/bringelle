#include "framebuffer.h"

#define MAX_COL 80
#define MAX_LINE 25

VIDEO_STATE VS={
  (u8 *)0xB8000,
  0,
  0,
  BLACK,
  GRAY,
};

void putchar(char c){
  // Handle newline here
  if(c=='\n'){
    VS.col=0;
    VS.line+=1;
    if(VS.line>=MAX_LINE){
      VS.line=MAX_LINE-1;
      scrollup();
    }
    return;
  }
  
  // Print char
  VS.mem[VS.col*2+MAX_COL*VS.line*2]=c;
  VS.mem[VS.col*2+MAX_COL*VS.line*2+1]=VS.fg|VS.bg<<4;
  
  // Refresh location
  VS.col+=1;
  if(VS.col>= MAX_COL){
    VS.col=0;
    VS.line+=1;
    if(VS.line>=MAX_LINE){
      VS.line=MAX_LINE-1;
      scrollup();
    }
  }
}

void clear(){
  for(char i=0;i<MAX_LINE;i++){
    scrollup();
  }
}

void scrollup(){
  // Move VS.line up
  for(char i=1;i<=MAX_LINE;i++){
    for(char j=0;j<=MAX_COL;j++)
      VS.mem[j*2+MAX_COL*(i-1)*2]=VS.mem[j*2+MAX_COL*i*2];
  }
  // Clear last VS.line
  for(char i=0;i<=MAX_COL;i++){
    VS.mem[i*2+MAX_COL*(MAX_LINE-1)*2]='\0';
  }
}
