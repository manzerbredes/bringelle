#include "8042.h"
#include "drivers/framebuffer.h"
#include "core/asm.h"

DEFINE_AZERTY;

void _8042_keypress(){
    u8 data;
    do {
        inb(0x64,data);
    }
    while((data&0x01) == 0);
    inb(0x60,data);
    if(data<0x80)
        putchar(AZERTY[data]);
}
