#include "pic.h"
#include "core/asm.h"
#include "core/mem.h"
#include "core/syscall.h"

void pic_init(){
    
    // ICW1: Initialisation
    outbj(0x20,0x11); // Master
    outbj(0xA0,0x11); // Slave

    // ICW2: Map IRQ index to entry into the IDT
    outbj(0x21,0x20); // Start interrupt at offset 0x20 in IDT (index 32)
    outbj(0xA1,0x70); // Start interrupt at offset 0x50 in IDT (index 80)

    // ICW3: Indicate the connection between master and slave
    outbj(0x21,0x02); // Slave connected to pin 2
    outbj(0xA1,0x01); // Indicate pin id to the slave (2-1)

    // ICW4: Operating mode
    outbj(0x21,0x01); // Default operating mode
    outbj(0xA1,0x01); // Default operating mode

    // OCW: Masking
    outbj(0x21,0b11111100);
}


