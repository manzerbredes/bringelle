#include "multiboot.h"

/// See boot.S
extern u8* MB_INFO;

char mb_load_tag(char **data, char type){
    char *c_info_size=MB_INFO;
    char *c_tag_type=c_info_size+8;
    char *c_tag_size=c_info_size+12;

    int max_size=*((int*)c_info_size);
    while(((int)c_tag_type-(int)MB_INFO)<max_size){       
        int tag_type=*((int*)c_tag_type);
        int tag_size=*((int*)c_tag_size);
        if(tag_type==type){
            *data=c_tag_type;
            return 0;
        }

        c_tag_type=c_tag_type+tag_size+4;
        // Skip padding for 64 bits
        int p=(int)c_tag_type;
        while((p & 0x7) != 0)
            p++;
        // Assign address after padding
        c_tag_type=(char *)p;
        c_tag_size=c_tag_type+4;

    }
    return 1;
}
char mb_load_bl_name(MBI_TAG_BL_NAME *data){
    char *to_load;
    if(!mb_load_tag(&to_load,2)){
        memcpy(to_load,data,8);
        to_load+=8;
        data->name=to_load;
        return 0;
    }
    return 1;
}

char mb_load_fb(MBI_TAG_FB *data){
    char *to_load;
    if(!mb_load_tag(&to_load,8)){
        asm("mov %0, %%ecx;aa:;jmp aa;"::"r"(to_load));
        memcpy(to_load,data,8);
        to_load+=8;
        memcpy(to_load,&(data->framebuffer_addr),8);
        return 0;
    }
    return 1;
}
