#ifndef MULTIBOOT_H
#define MULTIBOOT_H

#include "core/types.h"
#include "core/mem.h"

typedef struct MBI_TAG_HEADER {
    u32 type;
    u32 size;
} __attribute__((packed)) MBI_TAG_HEADER;

typedef struct MBI_TAG_BL_NAME {
    MBI_TAG_HEADER header;
    u8 *name;
} __attribute__((packed)) MBI_TAG_BL_NAME;

typedef struct MBI_TAG_FB {
    MBI_TAG_HEADER header;
    u64 framebuffer_addr;
    u32 framebuffer_pitch;
    u32 framebuffer_width;
    u32 framebuffer_height;
    u8 framebuffer_bpp;
    u8 framebuffer_type;
    u8 reserved;
    u8 *color_infos;
}__attribute__((packed)) MBI_TAG_FB;

/**
 * Parse Bootloader boot information structure
 */
char mb_load_tag(char **data, char type);

/**
 * Search for Bootloader name
 */
char mb_load_bl_name(MBI_TAG_BL_NAME *data);

/**
 * Search for FrameBuffer structure (TODO: Finish)
 */
char mb_load_fb(MBI_TAG_FB *data);

#endif