.globl  _start
.globl MB_INFO
.extern bringelle
.extern gdt_memcpy
.extern mb_load_fb_tag
.extern _bss_start
.extern _bss_end

.set STACK_LOCATION, 0x1FFFFF

.section .multiboot

.set MB_MAGIC, 0xE85250D6
.set MB_ARCH, 0x00000000
.set MB_HEADER_LENGTH, (mb_header_end-mb_header_start)
.set MB_CHECKSUM, -(MB_MAGIC+MB_ARCH+MB_HEADER_LENGTH)

mb_header_start:
.align 8
.int MB_MAGIC
.int MB_ARCH
.int MB_HEADER_LENGTH
.int MB_CHECKSUM
# ----------- Address tag
.align 8
.short 2
.short 0 # Not optional
.int 24  # Size
.int mb_header_start # header_addr
.int mb_header_start # load_addr
.int 0x0 # load_end_addr
.int 0x0 # bss_end_addr
# ----------- Addr tag
.align 8
.short 0x3
.short 0
.int 12
.int _start
# ----------- End tag
.align 8
.int 0x0
.int 0x8
mb_header_end:

.section .text

MB_INFO:
 .int 0xABCDEF # Will contains the Multiboot2 information data structure address

_start:
mov %ebx, (MB_INFO) # Store Bootloader informations address

# Copy GDT into memory then load its register
call gdt_memcpy
lgdtl (GDTR)

# Update all segments register
# with the new GDT offset
movw $0x10, %ax
movw %ax, %ds
movw %ax, %es
movw %ax, %fs
movw %ax, %gs

# Update code segment
ljmp $0x08, $cs_new
cs_new:

# Update stack segment
movw $0x18, %ax
movw %ax, %ss
movl $STACK_LOCATION,%esp

# Load 
mov $0x38, %eax
ltr %ax

# Zeroing the .bss section
mov $_bss_start, %eax
mov $_bss_end, %ebx
start_zeroing:
    movb $0x0, (%eax)
    cmp %eax, %ebx
    je end_zeroing
    inc %eax
    jmp start_zeroing
end_zeroing:

# Start kernel main function
call bringelle

