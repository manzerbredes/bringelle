#ifndef IDT_H
#define IDT_H

#include "core/types.h"
#include "core/mem.h"

#define IDT_MAX_ENTRY 200
#define IDT_INT_GATE 0x0E00
#define IDT_TRAP_GATE 0x0F00
#define IDT_P 0x8000
#define IDT_PRVL_0 0x0000
#define IDT_PRVL_1 0x2000
#define IDT_PRVL_2 0x4000
#define IDT_PRVL_3 0x6000

typedef struct IDT_ENTRY {
    u16 segment;
    u32 offset;
    u16 type;
} IDT_ENTRY;

struct IDT_REGISTER {
    u16 limit;
    u32 base;
} __attribute__((packed));

/**
 * Copy IDT into memory and load it
 */
void idt_init();

/**
 * Write an IDT entry into memory
 */
void idt_write_entry(IDT_ENTRY entry,int id);

#endif