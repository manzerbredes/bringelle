#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "mem.h"

#define MAX_PROC 10

// If you change the following struct
// please consider updating the code in scheduler_asm.S
typedef struct REGISTERS {
    u32 eax, ebx, ecx, edx;
    u32 cs, eip;
    u32 ss, esp, ebp;
    u32 esi, edi;
    u32 ds, es, fs, gs;
    u32 eflags;
    u32 ss0, esp0;
} __attribute__((packed)) REGISTERS;

// If you change the following struct
// please consider updating the code in scheduler_asm.S
typedef struct PROC {
    u16 id;
    u16 pid;
    u32* page_dir;
    REGISTERS regs;
} __attribute__((packed)) PROC;

extern char show_tics;
extern char scheduler_on;
extern PROC procs[MAX_PROC];
extern u16 current_id; // Current running task PID/id
extern u16 nproc; // Number of active tasks

/**
 * Must be called at each clock interrupt
 */
void clock();

/**
 * Called by clock() and schedule the next task
 * Stack is a pointer pointing to the gs register on the stack.
 * The stack must contains the interrupted process registers in the following
 * order: gs,fs,es,ds,edi,esi,ebp,UNUSED,edx,ecx,ebx,eax,eip,cs,eflags,esp,ss
 */
void schedule(u32 *stack);

/**
 * Create a new task to be schedule
 */
void task_create(void *task, int task_size);

/**
 * Stack the scheduler starting by task with PID 0
 */
void scheduler_start();

#endif