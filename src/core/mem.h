#ifndef MEM_H
#define MEM_H

#include "types.h"

/**
 * Copy size byte from *src to *dst
 */
void memcpy(void *src, void *dst, int size);

#endif
