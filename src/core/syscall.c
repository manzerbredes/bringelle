#include "syscall.h"
#include "gdt.h"
#include "libc/stdio.h"

void syscall(){
    int call_number;
    asm("movl %%eax, %0":"=m"(call_number));
    if(call_number==1){
        int msg_addr;
        asm("movl %%ebx, %0":"=m"(msg_addr));
        char *msg=(char*)msg_addr;
        print(msg);
    }
    else{
        print("Syscall ");
        printi(call_number);
        print(" unknown");
    }
}