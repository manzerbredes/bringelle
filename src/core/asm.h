#ifndef ASM_H
#define ASM_H

#define outb(port,value) \
    asm volatile ("outb %%al, %%dx" :: "a"(value), "d" (port) )

#define outbj(port,value) \
    asm volatile ("outb %%al, %%dx;" :: "a" (value), "d"(port) )

#define inb(port,dst) \
    asm volatile ("inb %%dx, %%al": "=a" (dst) : "d" (port))

#endif