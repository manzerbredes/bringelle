#ifndef PAGING_H
#define PAGING_H

#define PAGING_CR0_BIT 0x80000000
#define PAGING_PAGE_SIZE 4096
#define PAGING_MAX_PAGES 2048 // At least 1024 for the kernel
#if PAGING_MAX_PAGES%1024>0
    #define PAGING_MAX_DIR_ENTRY PAGING_MAX_PAGES/1024+1
#else
    #define PAGING_MAX_DIR_ENTRY PAGING_MAX_PAGES/1024
#endif
#define PAGING_PADDR(entry) ((int*)(((u32)entry)&0xFFFFF000))
#define PAGING_ENTRY_POINT_VIRT (1024*PAGING_PAGE_SIZE)
#define PAGING_ENTRY_POINT_PHY(page_dir) ((int*)(((int*)((((int*)page_dir)[1])&0xFFFFF000))[0]&0xFFFFF000))

/**
 * Configure and enable paging
 */
void paging_enable();

/**
 * Allocate a new page and return its address
 */
char* paging_allocate_next_page();

/**
 * Set usage status of a page
 */
void paging_set_usage(int addr,char state);

/**
 * Create a new page directory containing
 * p pages.
 */
int *paging_allocate(int p);

/**
 * Simple dump
 */
void paging_dump(int min,int max);

/**
 * Handler of page fault
 */
void paging_page_fault();

#endif