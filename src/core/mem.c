#include "mem.h"

void memcpy(void *src, void *dst, int size){
  u8 *char_src=(u8 *)src;
  u8 *char_dst=(u8 *)dst;
  for(int i=0;i<size;i++)
    char_dst[i]=char_src[i];
}
