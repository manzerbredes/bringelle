#include "libc/stdio.h"
#include "boot/multiboot.h"
#include "core/mem.h"
#include "core/gdt.h"
#include "core/paging.h"
#include "core/scheduler.h"

#define TASK_WAIT 50000000

extern void interrupt_enable();

void utask(){
  char *msg=(char*)PAGING_ENTRY_POINT_VIRT+300;
  msg[0]='A';
  msg[1]='\0';
  while(1){
      asm("mov $0x1, %%eax;int $0x30"::"b"(msg));
      for(int i=0;i<TASK_WAIT;i++){

      }
  }
}

void utask2(){
  char *msg=(char*)PAGING_ENTRY_POINT_VIRT+300;
  msg[0]='B';
  msg[1]='\0';
  while(1){
      asm("mov $0x1, %%eax;int $0x30"::"b"(msg));
      for(int i=0;i<TASK_WAIT;i++){
        
      }
  }
}

/**
 * Kernel entry point
 */
void bringelle(){
  clear();
  printc("Booting Bringelle...\n",GREEN);

  // ----- Kernel boot sequence
  interrupt_enable();
  print("Interrupts enabled\n");

  paging_enable();
  print("Paging enable\n");
  print("Kernel started!\n");
  print("-----------------------\n");
  show_tics=1;

  // Utask
  print("Launch user tasks \n");

  task_create(utask,100);
  task_create(utask2,100);


  scheduler_start();

  while(1);
}


