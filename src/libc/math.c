#include "math.h"

int pow(int x,int n){
    if(n<0)
        return -1;
    else if(n==0)
        return 1;
    else if(n==1)
        return x;
    int ret=x;
    for(int i=0;i<(n-1);i++)
        ret*=x;
    return ret;
}

int max(int x,int y){
    if(x>y)
        return x;
    return y;
}

int min(int x,int y){
    if(x<y)
        return x;
    return y;
}

int abs(int x){
    if(x<0)
        return -x;
    return x;
}