#include "stdio.h"
#include "string.h"

extern VIDEO_STATE VS;

void print(char *str){
  int i=0;
  while(str[i]!='\0'){
    putchar(str[i]);
    i++;
  }
}

void printc(char* str,VIDEO_COLORS c){
  VIDEO_COLORS backup=VS.fg;
  VS.fg=c;
  print(str);
  VS.fg=backup;
}

void printi(int i){
    char str[12];
    itoa(i,str);
    print(str);
}