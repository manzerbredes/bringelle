#ifndef STDIO_H
#define STDIO_H

#include "drivers/framebuffer.h"

/**
 * Print a char* in the framebuffer
 */
void print(char*);

/**
 * Print a char in the framebuffer
 */
void printc(char*,VIDEO_COLORS c);

/**
 * Print an integer using itoa()
 */
void printi(int i);

#endif