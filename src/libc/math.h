#ifndef MATH_H
#define MATH_H

int pow(int x,int n);
int max(int x,int y);
int min(int x,int y);
int abs(int x);

#endif